<!--====== NAVBAR TWO PART START ======-->

<section class="navbar-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg">

                <a class="navbar-brand" href="{{url('/')}}">
                        <img src="{{asset('assets/Smash/images/logo.svg')}}" alt="Logo">
                    </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTwo"
                        aria-controls="navbarTwo" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse sub-menu-bar" id="navbarTwo">
                        <ul class="navbar-nav m-auto">
                            <li class="nav-item active"><a class="page-scroll" href="{{url('/')}}">Home</a></li>
                            <li class="nav-item"><a class="page-scroll" href="#">Services</a></li>
                            <li class="nav-item"><a class="page-scroll" href="#">Portfolio</a></li>
                            <li class="nav-item"><a class="page-scroll" href="#">Pricing</a></li>
                            <li class="nav-item"><a class="page-scroll" href="#">About</a></li>
                            <li class="nav-item"><a class="page-scroll" href="#">Team</a></li>
                        </ul>
                    </div>

                    <div class="navbar-btn d-none d-sm-inline-block">
                        <ul>
                            <li><a class="solid" href="{{route('administrator')}}">Login</a></li>
                        </ul>
                    </div>
                </nav> <!-- navbar -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</section>

<!--====== NAVBAR TWO PART ENDS ======-->
