<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">

    <!--====== Title ======-->
    <title>Smash - Bootstrap Business Template</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{asset('assets/Smash/images/favicon.png')}}" type="image/png">

    <!--====== Magnific Popup CSS ======-->
    <link rel="stylesheet" href="{{asset('assets/Smash/css/magnific-popup.css')}}">

    <!--====== Slick CSS ======-->
    <link rel="stylesheet" href="{{asset('assets/Smash/css/slick.css')}}">

    <!--====== Line Icons CSS ======-->
    <link rel="stylesheet" href="{{asset('assets/Smash/css/LineIcons.css')}}">

    <!--====== Bootstrap CSS ======-->
    <link rel="stylesheet" href="{{asset('assets/Smash/css/bootstrap.min.css')}}">

    <!--====== Default CSS ======-->
    <link rel="stylesheet" href="{{asset('assets/Smash/css/default.css')}}">

    <!--====== Style CSS ======-->
    <link rel="stylesheet" href="{{asset('assets/Smash/css/style.css')}}">

</head>

<body>
    <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  @yield('homepage')





    <!--====== Jquery js ======-->
    <script src="{{asset('assets/Smash/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('assets/Smash/js/vendor/modernizr-3.7.1.min.js')}}"></script>

    <!--====== Bootstrap js ======-->
    <script src="{{asset('assets/Smash/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/Smash/js/bootstrap.min.js')}}"></script>

    <!--====== Slick js ======-->
    <script src="{{asset('assets/Smash/js/slick.min.js')}}"></script>

    <!--====== Magnific Popup js ======-->
    <script src="{{asset('assets/Smash/js/jquery.magnific-popup.min.js')}}"></script>

    <!--====== Ajax Contact js ======-->
    <script src="{{asset('assets/Smash/js/ajax-contact.js')}}"></script>

    <!--====== Isotope js ======-->
    <script src="{{asset('assets/Smash/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>

    <!--====== Scrolling Nav js ======-->
    <script src="{{asset('assets/Smash/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('assets/Smash/js/scrolling-nav.js')}}"></script>

    <!--====== Main js ======-->
    <script src="{{asset('assets/Smash/js/main.js')}}"></script>

</body>

</html>
