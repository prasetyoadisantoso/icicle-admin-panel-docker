@extends('adminlte.layouts.auth')

@section('content-login')
<div class="form-box" id="login-box">
    <div class="header">Sign In</div>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="body bg-gray">
            <div class="form-group">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email...">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong style="color: red;">{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="current-password" placeholder="Password...">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong style="color: red;">{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input class="form-check-input" type="checkbox" name="remember" id="remember"
                    {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
            </div>
        </div>
        <div class="footer">
            <button type="submit" class="btn bg-olive btn-block">Sign me in</button>

            {{-- @if (Route::has('password.request')) --}}
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
            {{-- @endif --}}
            <a class="btn btn-link" href="{{ route('register') }}">
                {{ __('Register') }}
            </a>
        </div>
    </form>

    <div class="margin text-center">
        <span>Created with ❤ by </span><a href="https://facebook.com/pras.element">Prasetyo Adi Santoso</a>
        <br /><br />
        <a href="{{url('/')}}" class="btn btn-primary">Go to Homepage</a>
    </div>

</div>

@endsection
