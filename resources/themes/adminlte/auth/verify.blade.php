@extends('adminlte.layouts.auth')

@section('content-verify')
<div class="container">

    <div class="form-box" id="login-box">
        <div class="header">{{ __('Verify Your Email Address') }}</div>

        <div class="body">
            @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('A fresh verification link has been sent to your email address.') }}
            </div>
            @endif

            {{ __('Before proceeding, please check your email for a verification link.') }}
            {{ __('If you did not receive the email') }},


            <form class="d-inline text-center" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <br>
                <button type="submit" class="btn btn-secondary">{{ __('click here to request another') }}</button>.
            </form>
            <br>
        </div>
    </div>

</div>
@endsection
