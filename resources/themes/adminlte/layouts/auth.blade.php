<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        {{-- <title>AdminLTE | Log in</title> --}}
        <title>Login</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {{-- <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <link href="{{asset('assets/AdminLTE/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        {{-- <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" /> --}}
        <link href="{{asset('assets/AdminLTE/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        {{-- <link href="../../css/AdminLTE.css" rel="stylesheet" type="text/css" /> --}}
        <link href="{{asset('assets/AdminLTE/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">
        <div id="app">
            @yield('content-login')
            @yield('content-register')
            @yield('content-verify')
            @yield('content-email')
            @yield('content-reset')
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="{{asset('assets/AdminLTE/js/jquery-2.0.2.min.js')}}"></script>
        <!-- Bootstrap -->
        <script src="{{asset('assets/AdminLTE/js/bootstrap.min.js')}}" type="text/javascript"></script>
        {{-- <script src="../../js/bootstrap.min.js" type="text/javascript"></script> --}}

    </body>
</html>
