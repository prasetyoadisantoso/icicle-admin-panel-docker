@extends('default.layouts.layout')

@section('posts')
@include('default.navbar')

{{-- Get class from Modules CMS --}}
@php
use \Modules\Cms\Classes\PostsClass;
$post = PostsClass::getPosts();
@endphp

{{-- Responsive Image --}}
<style>
    img {
        width: 50% !important;
        height: 100% !important;
    }
</style>
{{-- End Responsive Image --}}



<section class=" masthead page-section portfolio" id="portfolio">
    <div class="container">

        <!-- Posts Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-grey">Posts List</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star text-custom"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <br>

        {{-- Post List Item --}}
        <div class="row">
            @foreach ($post as $item)
            {{-- Post Item --}}
            <div class="col-sm-12 col-12 mb-5">
                <div class="card">
                    <div class="card-header text-center">
                        <h1>{!!$item->title!!}</h1> <small>{!!$item->created_at!!}</small>
                    </div>
                    <div class="card-body">
                        {!!$item->content!!}
                    </div>
                </div>
            </div>
            {{-- End Post Item --}}
            @endforeach
        </div>
        {{-- End Post List Item --}}

    </div>
</section>

@endsection
