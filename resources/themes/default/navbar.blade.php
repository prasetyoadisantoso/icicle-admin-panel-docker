<!-- Navigation-->
<nav class="navbar navbar-expand-lg bg-navbar text-uppercase fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{url('/')}}"> <i class="fas fa-home text-light"></i> Home</a>
        <button
            class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded"
            type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
            aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">

                {{-- This is for New Front-End Modules --}}
                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="#">Front End Modules</a>
                </li>

                {{-- If user roles empty show it --}}
                @if (empty($user))
                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="{{route('login')}}">Login</a></li>

                {{-- If user roles Administrator show it --}}
                @elseif ($user->roles()->pluck('name')[0] == 'Administrator')

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="{{route('administrator')}}">Dashboard</a></li>


                {{-- If user roles not administrator show it --}}
                @elseif ($user->roles()->pluck('name') != 'Administrator')
                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="{{url('administrator/logout')}}">Logout</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
