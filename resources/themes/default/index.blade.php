@extends('default.layouts.layout')

@section('homepage')
@include('default.navbar')
<!-- Masthead-->
<header class="masthead text-dark text-center">
    <div class="container d-flex align-items-center flex-column">
        <!-- Masthead Heading-->
        <h1 class="masthead-heading text-uppercase mb-5 text-grey"> Welcome </h1>
        <!-- Masthead Avatar Image-->
        <img class="masthead-avatar mb-0" src="{{asset('assets/default/img/icicle-logo.png')}}" alt="" />
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star text-custom"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Masthead Subheading-->
        <p class="masthead-subheading font-weight-light mb-0 text-grey">Your Foundation to Building Project</p>
    </div>
</header>

<hr>


<!-- About Section-->
<section class="page-section text-grey mb-0" id="about">
    <div class="container">
        <!-- About Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-grey">What is it... ?</h2>
        <!-- Icon Divider-->
        <div class="divider-custom text-grey">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star text-custom"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- About Section Content-->
        <div class="row">
            <div class="col-lg-4 ml-auto">
                <p class="lead">ICICLE Administrator Panel is a website application based on <br>Laravel 8 as a
                    foundation.</p>
            </div>
            <div class="col-lg-4 mr-auto">
                <p class="lead">We build this website application using template from StartBootstrap, AdminLTE, Smash
                    Template and some package from <a href="https://packagist.org/" class="text-custom">Packagist</a> for
                    Back-End. </p>
            </div>
        </div>
    </div>
</section>






<!-- Footer-->
<footer class="footer text-center">
    <div class="container">
        <div class="row">
            <!-- Footer Location-->
            <div class="col-lg-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Maintainer</h4>
                <img class="mb-0 ml-2 rounded-circle" src="{{asset('assets/default/img/pras.jpg')}}" alt="" width="80" />
                <p class="lead text-center mb-0">
                    Prasetyo Adi Santoso
                    <br />
                    Main Developer
                </p>
            </div>
            <!-- Footer Social Icons-->
            <div class="col-lg-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Around the Web</h4>
                <a class="btn btn-outline-light btn-social mx-1" href="https://www.facebook.com/pras.element"><i class="fab fa-fw fa-facebook-f"></i></a>
                <a class="btn btn-outline-light btn-social mx-1" href="https://instagram.com/purasutiyo"><i class="fab fa-fw fa-instagram"></i></a>
                {{-- <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-linkedin-in"></i></a --}}
                <a class="btn btn-outline-light btn-social mx-1" href="https://gitlab.com/prasetyo.element/"><i class="fab fa-fw fa-gitlab"></i></a>
            </div>
            <!-- Footer About Text-->
            <div class="col-lg-4">
                <h4 class="text-uppercase mb-4">License</h4>
                <p class="lead mb-0">
                    ICICLE Administrator Panel is a free to use, MIT licensed created by
                    <a href="http://elementdeveloper.com">Personal</a>
                    .
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Copyright Section-->
<div class="copyright py-4 text-center text-white">
    <div class="container"><small>Copyright ©Icicle Administrator Panel 2020</small></div>
</div>
<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
<div class="scroll-to-top d-lg-none position-fixed">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i
            class="fa fa-chevron-up"></i></a>
</div>


@endsection
