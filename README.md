<p align="center"><img src="https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/raw/master/Icicle%20Admin%20Panel.png" width="150"></p>

<p align="center">
<a href="https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/releases/beta-7.8.15"><img src="https://img.shields.io/badge/version-beta--7.8.15-green" alt="Version"></a>
<a href="#"><img src="https://img.shields.io/badge/releases-stable-success" alt="Version"></a>
<a href="https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/blob/master/LICENSE"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
<a href="https://gitlab.com/prasetyo.element/icicle-admin-panel-docker/-/commits/master"><img alt="pipeline status" src="https://gitlab.com/prasetyo.element/icicle-admin-panel-docker/badges/master/pipeline.svg" /></a>
<a href="#"><img src="https://img.shields.io/badge/php-%5E7.3-blue" alt="Php Version"></a>

</p>

# ICICLE Administrator Panel with Docker

This is Docker Version of [ICICLE Administrator Project](https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/).


## Installation
1. Install docker
2. Copy .env.example to .env
3. change database setting in .env like this
<br>`DB_CONNECTION=mysql`
<br>`DB_HOST=db`
<br>`DB_PORT=3306`
<br>`DB_DATABASE=db`
<br>`DB_USERNAME=dbuser`
<br>`DB_PASSWORD=secret`  
4. Just run this 
<br>`docker-compose build && docker-compose up -d && docker-compose logs -f`

Username (Admin) : admin@icicle.io <br />
Username (Member) : member@icicle.io <br />
Password : 123456 <br />

**Note** : Activated all permissions to gain all control system at administration dashboard panel

### Setting Email with SMTP Gmail for Registration Verification
1. Activated [Google App Password](https://myaccount.google.com/apppasswords).
2. Create your app password and copy the password has been given.
3. If you want to set email with gmail account you can edit this code to your .env
<br>`MAIL_HOST=smtp.google.com`
<br>`MAIL_PORT=587`
<br>`MAIL_USERNAME=youremail@gmail.com`
<br>`MAIL_PASSWORD=yourapppassword`
<br>`MAIL_ENCRYPTION=tls` 

If you want to try this demo, [Click Here!](https://icicle-project.elementdeveloper.com/).

## License

ICICLE Administrator Panel is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
