<?php

// namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ThemeTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert basic theme name here
        DB::table('settings')->insert(
            [
                'id' => '1',
                'themes' => 'default'
            ]
        );
    }
}
