<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        // Place all class in seeder here
        $this->call([
            UserTableSeed::class,
            PermissionTableSeeder::class,
            RoleTableSeed::class,
            RolePermissionTableSeed::class,
            ModelRoleTableSeed::class,
            ThemeTableSeed::class,

        ]);
    }
}
