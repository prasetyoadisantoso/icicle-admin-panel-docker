<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert dummy users for admin and member here
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@icicle.io',
                'email_verified_at' => date("Y-m-d H:i:s"),
                'password' => bcrypt('123456'),
            ],

            [
                'name' => 'member',
                'email' => 'member@icicle.io',
                'email_verified_at' => date("Y-m-d H:i:s"),
                'password' => bcrypt('123456'),
            ],


        ]);
    }
}
