<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert dummy roles here
        DB::table('roles')->insert([
            [
                'name' => 'Administrator',
                'guard_name' => 'web'
            ],

            [
                'name' => 'Member',
                'guard_name' => 'web'
            ],


        ]);

    }
}
