<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Settings\Entities\Settings;
use App\User;

//Redirect to Blade at resources/themes
class MainController extends Controller
{

    public function index()
    {

        $user = auth()->user();

        // Get Themes from database
        $themes_now =  Settings::first();
        $themes = ($themes_now->themes);
        return view($themes . '.index', compact('user'));
    }

    public function posts()
    {
        $user = auth()->user();
        $themes_now =  Settings::first();
        $themes = ($themes_now->themes);
        return view($themes . '.posts', compact('user'));
    }
}
