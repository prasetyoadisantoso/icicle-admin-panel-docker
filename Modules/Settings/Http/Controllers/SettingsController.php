<?php

namespace Modules\Settings\Http\Controllers;

// use App\Settings;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
// use Illuminate\Routing\Controller;
use Modules\Settings\Http\Controllers\Controller;
use Modules\Settings\Entities\Settings;
use Modules\Products\Entities\Products;
use Illuminate\Support\Facades\Auth;
use DB;

class SettingsController extends Controller
{

    // Gateway Permissions
    public $roles;
    function __construct()
    {
        $this->middleware('permission:setting-create');
        $this->middleware('permission:setting-edit');
        $this->middleware('permission:setting-delete');
        $this->middleware('auth');
        $this->roles = Auth::user();
    }


    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    // Route to Index Settings Page
    public function index()
    {
        return view('settings::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('settings::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('settings::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('settings::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }



    // Set themes from form request
    public function theme(Request $request)
    {
        //
        $this->validate($request, [
            'themes' => 'required',
        ]);

        $input = $request->all();
        $inputs = $input['themes'];

        Settings::where('id',1)->update(['themes'=> $inputs]);

        return redirect()->route('settings')
                        ->with('success','Theme sets!');
    }
}
