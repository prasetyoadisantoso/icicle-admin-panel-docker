<?php

namespace Modules\Settings\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Modules\Settings\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CacheController extends Controller
{

    // Gateway Permission
    public $roles;
    function __construct()
    {
        $this->middleware('permission:setting-create');
        $this->middleware('permission:setting-edit');
        $this->middleware('permission:setting-delete');
        $this->middleware('auth');
        $this->roles = Auth::user();
    }


    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    // Route to Index Cache Management Page
    public function index()
    {
        return view('settings::cache.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    // Clear Config
    public function configClear()
    {
        Artisan::call('config:clear');
        return redirect()->route('cache')->with('success','Config cleared!');;
    }


    // Clear Cache
    public function cacheClear()
    {
        Artisan::call('cache:clear');
        return redirect()->route('cache')->with('success','Cache cleared!');;
    }


    // Clear view
    public function viewClear()
    {
        Artisan::call('view:clear');
        return redirect()->route('cache')->with('success','Views cleared!');;
    }


    // Clear Route
    public function routeClear()
    {
        Artisan::call('route:clear');
        return redirect()->route('cache')->with('success','Routes cleared!');;
    }



}
