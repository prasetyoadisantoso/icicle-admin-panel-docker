@extends('adminlte.layouts.master')

@section('content-settings')
@include('administrator::sidebar')

<aside class="right-side">
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Administration Settings</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        {{-- Form Change Theme --}}
        <div class="box">
            {!! Form::open(array('route' => 'theme.store','method'=>'POST')) !!}
            <div class="row">
                <div class="col-md-4 padding-box">
                    <div class="form-group">
                        <strong>Theme:</strong>
                        {!! Form::select('themes', ['default' => 'Default', 'smash' => 'Smash', ],null, array('class' =>
                        'form-control')); !!}
                    </div>
                    <div class="mt-1 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
</aside>

<style>
    .padding-box {
        padding-right: 5%;
        padding-left: 5%;
        padding-top: 5%;
        padding-bottom: 5%;
    }
</style>




@endsection
