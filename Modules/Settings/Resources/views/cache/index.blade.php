@extends('adminlte.layouts.master')


@section('content-cache')
@include('administrator::sidebar')

<aside class="right-side">

    {{-- Role Management Header --}}
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <h1>
                    Cache Management
                    <small>&nbsp; Clear redundant cache</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('roles')}}"><i class="fa fa-dashboard"></i> Roles</a></li>
                    {{-- <li class="active">Users</li> --}}
                </ol>
            </div>
        </div>
    </section>

    <!-- Modal -->
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <section class="content">
        <div class="box">
            {{-- Responsive Table --}}
            <div class="box-body table-responsive">
                <table id="example1" class="example1 table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Command List</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Clear config | command:"config:clear"</td>
                            <td>
                                <a class="btn btn-warning" href="{{ route('config-clear') }}"> Config Clear</a>
                            </td>
                        </tr>
                    </tbody>

                    <tbody>
                        <tr>
                            <td>2</td>
                            <td>Clear cache | command:"cache:clear"</td>
                            <td>
                                <a class="btn btn-warning" href="{{ route('cache-clear') }}"> Cache Clear</a>
                            </td>
                        </tr>
                    </tbody>

                    <tbody>
                        <tr>
                            <td>3</td>
                            <td>Clear view | command:"view:clear"</td>
                            <td>
                                <a class="btn btn-warning" href="{{ route('view-clear') }}"> View Clear</a>
                            </td>
                        </tr>
                    </tbody>

                    <tbody>
                        <tr>
                            <td>3</td>
                            <td>Clear routes | command:"route:clear"</td>
                            <td>
                                <a class="btn btn-warning" href="{{ route('route-clear') }}"> Route Clear</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</aside>


@endsection
