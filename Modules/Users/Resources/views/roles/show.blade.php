{{-- Show Detail Roles with Permission --}}
<div class="modal-header">
    <h2> Show Role</h2>
</div>


<div class="modal-body">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {{ $role->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Permissions:</strong>
            @if(!empty($rolePermissions))
                @foreach($rolePermissions as $v)
                    <label class="label label-success">{{ $v->name }},</label>
                @endforeach
            @endif
        </div>
    </div>
</div>

<div class="text-center">
    <a class="btn btn-primary" href="{{ route('roles') }}"> Back</a>
</div>
