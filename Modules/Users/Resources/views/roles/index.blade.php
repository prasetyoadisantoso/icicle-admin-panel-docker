@extends('adminlte.layouts.master')

@section('content-roles')
@include('administrator::sidebar')

<aside class="right-side">

    {{-- Role Management Header --}}
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <h1>
                    Role Management
                    <small>&nbsp; Manage Role Permissions</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('roles')}}"><i class="fa fa-dashboard"></i> Roles</a></li>
                    {{-- <li class="active">Users</li> --}}
                </ol>
            </div>
        </div>
    </section>



    {{-- Button Create Roles --}}
    <section class="content">
        <div class="pull-left">

            @can('role-create')
            <a class="btn btn-success" href="{{ route('roles.create') }}" data-toggle="modal"
                data-target="#createModal"> Create New Role</a>
            @endcan

            <!-- Modal -->
            <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true"
                style="background-color: white; margin-top:5%; margin-bottom: 5%; margin-left:10%; margin-right:10%; border-style: hidden;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    </div>
                </div>
            </div>

            {{-- @endcan --}}
        </div>
    </section>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif


    {{-- Table Roles --}}
    <section class="content">
        <div class="box">
            <div class="row box-header">
                <div class="col-md-8">
                    <h3 class="box-title">List Role</h3>
                </div><!-- /.box-header -->
                <div class="col-md-4">
                    <div class="pull-right" style="margin-top: -15px; margin-bottom: -15px;">
                        {{-- Pagination --}}
                        {!! $roles->render() !!}
                    </div>
                </div>
            </div>

            {{-- Responsive Table --}}
            <div class="box-body table-responsive">
                <table id="example1" class="example1 table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Roles Name</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                    @foreach ($roles as $key => $role)
                    <tbody>
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $role->name }}</td>
                            <td>
                                <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}" data-toggle="modal"
                                    data-target="#showModal">Show</a>
                                @can('role-edit')
                                <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}"
                                    data-toggle="modal" data-target="#editModal">Edit</a>
                                @endcan
                                @can('role-delete')
                                {!! Form::open(['method' => 'DELETE','url' => ['roles/delete',
                                $role->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                                @endcan
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>

            <!-- Edit Modal -->
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true"
                style="background-color: white; margin-top:5%; margin-bottom: 5%; margin-left:10%; margin-right:10%; border-style: hidden;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    </div>
                </div>
            </div>

            <!-- Show Modal -->
            <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true"
                style="background-color: white; margin-top:10%; margin-bottom: 10%; margin-left:10%; margin-right:10%; border-style: hidden;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>


@endsection
