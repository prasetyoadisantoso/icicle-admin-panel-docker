@extends('adminlte.layouts.master')


@section('content-users')

{{-- Show sidebar from administrator --}}
@include('administrator::sidebar')

<aside class="right-side">

    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <h1>
                    Users Management
                    <small>&nbsp; Manage User with Permission</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('users')}}"><i class="fa fa-dashboard"></i> Users</a></li>
                    {{-- <li class="active">Users</li> --}}
                </ol>
            </div>
        </div>
    </section>


    <section>
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    </section>


    <section class="content">

        {{-- Button Create User --}}
        <div class="pull-left">

            <form action="{{ route('users.create') }}" method="GET">
                {{-- @can('user-create') --}}
                <a class="btn btn-success" href="{{ route('users.create') }}" data-toggle="modal"
                    data-target="#exampleModal"> Create New User</a>
                @csrf
                {{-- @endcan --}}
            </form>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true"
                style="background-color: white; margin-top:5%; margin-bottom: 5%; margin-left:10%; margin-right:10%; border-style: hidden;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    </div>
                </div>
            </div>

            {{-- @endcan --}}
        </div>
    </section>


    <section class="content">
        <div class="box">
            <div class="row box-header">
                <div class="col-md-8">
                    <h3 class="box-title">List User</h3>
                </div><!-- /.box-header -->
                <div class="col-md-4">
                    <div class="pull-right" style="margin-top: -15px; margin-bottom: -15px;">
                        {{-- Pagination --}}
                        {!! $data->links() !!}
                    </div>
                </div>
            </div>

            {{-- Responsive Table --}}
            <div class="box-body table-responsive">
                <table id="example1" class="example1 table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                    @foreach ($data as $key => $user)
                    <tbody>
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                @if ($v == 'Member')
                                <label class="btn btn-warning btn-xs">{{ $v }}</label>
                                @else
                                <label class="btn btn-success btn-xs">{{ $v }}</label>
                                @endif
                                @endforeach
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-info" href="{{ route('users.show',$user->id) }}" data-toggle="modal"
                                    data-target="#showModal">Show</a>
                                <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}"
                                    data-toggle="modal" data-target="#editModal">Edit</a>
                                <!-- Edit Modal -->
                                <div class="modal rounded-2" id="editModal" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true"
                                    style="background-color: white; margin-top:2%; margin-bottom: 2%; margin-left:10%; margin-right:10%; border-style: hidden;">
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal rounded-2" id="showModal" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true"
                                    style="background-color: white; margin-top:15%; margin-bottom: 15%; margin-left:10%; margin-right:10%; border-style: hidden;">
                                </div>

                                {!! Form::open(['method' => 'DELETE','url' => ['users/delete',
                                $user->id],'style'=>'display:inline'])!!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>

        </div>
    </section>


</aside>

@endsection
