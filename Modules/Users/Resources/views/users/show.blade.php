{{-- Show Detail of User --}}

<div class="modal-header">
    <div class="col-lg-12 margin-tb">
        <div class="text-center">
            <h2> Show User</h2>
        </div>
    </div>
</div>


<div class="modal-body">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-center">
            <strong>Name:</strong>
            {{ $user->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-center">
            <strong>Email:</strong>
            {{ $user->email }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-center">
            <strong>Roles:</strong>
            @if(!empty($user->getRoleNames()))
                @foreach($user->getRoleNames() as $v)
                    <label class="badge badge-success">{{ $v }}</label>
                @endforeach
            @endif
        </div>
    </div>
    <div class="text-center">
        <a class="btn btn-primary" href="{{ route('users') }}"> Back</a>
    </div>
</div>
