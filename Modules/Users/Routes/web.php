<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Administrator Panel */
// Route to Users Management


// Route to Users Management Page
Route::prefix('users')->group(function() {
    Route::get('/', 'UsersController@index')->name('users');
    Route::get('create', 'UsersController@create')->name('users.create');
    Route::post('store', 'UsersController@store')->name('users.store');
    Route::get('edit/{id}', 'UsersController@edit')->name('users.edit');
    Route::get('show/{id}', 'UsersController@show')->name('users.show');
    Route::patch('update/{id}', 'UsersController@update');
    Route::delete('delete/{id}', 'UsersController@destroy');
});


// Route to Roles Management Page
Route::prefix('roles')->group(function() {
    Route::get('/', 'RolesController@index')->name('roles');
    Route::get('create', 'RolesController@create')->name('roles.create');
    Route::post('store', 'RolesController@store')->name('roles.store');
    Route::get('edit/{id}', 'RolesController@edit')->name('roles.edit');
    Route::get('show/{id}', 'RolesController@show')->name('roles.show');
    Route::patch('update/{id}', 'RolesController@update');
    Route::delete('delete/{id}', 'RolesController@destroy');
});
