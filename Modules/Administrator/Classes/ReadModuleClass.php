<?php
namespace Modules\Administrator\Classes;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class ReadModuleClass {

    use HasRoles;

    /* Logout Function */
    public static function read()
    {
        # code...
        $json = file_get_contents(base_path('modules_statuses.json'));
        $jsonLow = strtolower($json);
        $string = json_decode($jsonLow, true);
        $modules = collect($string);
        return $modules;
    }

}
