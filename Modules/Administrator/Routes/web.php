<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Administrator Panel*/
// Route to Administrator Page and Login Page
Route::prefix('administrator')->group(function() {

    Auth::routes(['verify' => true]);
    Route::get('/', 'AdministratorController@index')->name('administrator');

    // Function Register Form
    Route::get('register', 'Auth\RegisterController@showRegisterForm')->name('register');

    // Function Verify Form
    Route::get('email/verify', 'Auth\VerificationController@showVerifyForm')->name('verification.notice');

    // Function Password Reset Form
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');


    // Function Token Password
    Route::get('administrator/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.token');





    // Turn off register, password reset
    // Route::get('register', function (){
    //     return redirect('administrator/login');
    // });
    // Route::get('password/reset', function (){
    //     return redirect('administrator/login');
    // });

    // Route to logout
    Route::get('logout', 'AdministratorController@logout');
});
