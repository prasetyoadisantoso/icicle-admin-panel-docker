@php
    // Call Read Module Class Function
    use Modules\Administrator\Classes\ReadModuleClass;
    $module = ReadModuleClass::read();
@endphp

<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('assets/AdminLTE/img/avatar3.png')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, {{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i
                            class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="{{ url('administrator') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            {{-- THIS SECTION FOR NEW MODULE --}}

            {{-- Module List --}}
            <li class="treeview">
                <a href="#">
                    <i class="glyphicon glyphicon-th-large"></i>
                    <span>My Module</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @foreach ($module->slice(3) as $item => $status)
                    <li><a href="{{$item}}"><i class="fa fa-angle-double-right"></i>{{ucfirst($item)}}</a></li>
                    @endforeach
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>User Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/users')}}"><i class="fa fa-angle-double-right"></i>Users</a></li>
                    @can('role-create')
                    <li><a href="{{url('/roles')}}"><i class="fa fa-angle-double-right"></i>Roles</a></li>
                    @endcan
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>Administration</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('settings')}}"><i class="fa fa-angle-double-right"></i> Settings</a></li>
                    <li><a href="{{route('cache')}}"><i class="fa fa-angle-double-right"></i> Cache Management</a></li>
                </ul>
            </li>
        </ul>
        </li>

        </ul>
        <ul class="sidebar-menu">
            <li class="text-light">
                <a href="#" class="dropdown-toggle">
                    Laravel Version : {{ App::VERSION() }}
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
