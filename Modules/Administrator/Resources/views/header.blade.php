<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="{{ url('/') }}" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        {{config('app.name')}}
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span>{{Auth::user()->name}}<i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="{{asset('assets/AdminLTE/img/avatar3.png')}}" class="img-circle"
                                alt="User Image" />
                            <p>
                                {{Auth::user()->name}}
                            </p>
                            <b>
                                @foreach (auth()->user()->roles()->pluck('name') as $user)
                                Role: {{$user}}
                                @endforeach
                                {{-- <small>Member since Nov. 2012</small> --}}
                            </b>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">

                            <div class="text-center">
                                <a href="{{route('logout')}}" class="btn btn-default btn-flat" id="logout-form">Sign
                                    out</a>
                            </div>

                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>


</header>
