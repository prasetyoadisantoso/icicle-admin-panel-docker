<?php

namespace Modules\Administrator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Renderable;
// use Illuminate\Routing\Controller;
use Modules\Administrator\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Administrator\Classes\UserClass;


class AdministratorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    // roles permissions function
    public $roles;

    public function __construct()
    {
        $this->middleware('permission:product-list');
        $this->middleware(['auth','verified']);
        $this->roles = Auth::user();
    }



    /* Call function from class
    UserClass in
    Modules/Administrator/Classes
     */
    public function index()
    {
        if (!Auth::user()->hasRole('Administrator')) {
            return redirect('/');
        }

        $total_users = UserClass::getTotalUsers();
        return view('administrator::dashboard', compact('total_users'));
    }




    /* Logout Function */
    public function logout()
    {
        # code...
        Auth::logout();
        return redirect('/');
    }
}
