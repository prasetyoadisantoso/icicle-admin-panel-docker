<?php

namespace Modules\Administrator\Http\Controllers\Auth;

use Modules\Administrator\Http\Controllers\Controller;
use Modules\Administrator\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::Administrator;

    public function showResetForm(Request $request)
    {
        $token = $request->route()->parameter('token');

        return view('adminlte.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

}
