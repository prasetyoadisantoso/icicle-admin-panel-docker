# Release Notes


## [Stable Released - beta-7.8.11 (2020-09-25)](https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/releases/beta-7.8.11)
This is stable beta release for daily use.

### Change
- Tidying up the code
- Add some permission to database

### Fixed
- Some bugfix at alpha release




## [Unstable Released - alpha-7.6.5 (2020-09-19)](https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/releases/alpha-7.6.5)

### Added
- Basic CRUD Content Management System with CKEditor 
<br>

## [Unstable Released - alpha-7.5.5 (2020-09-18)](https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/releases/alpha-7.5.5)

### Added
- Cache Management System  
<br>


## [Unstable Released - alpha-7.4.5 (2020-09-18)](https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/releases/alpha-7.4.5)

### Added
- CRUD Management Roles

### Changed
- Reduce unused link

### Fixed
- Permission Roles Fix
- Database seeder  
<br>


## [Unstable Released - alpha-7.3.3 (2020-09-12)](https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/releases/alpha-7.3.3)

### Added
- CRUD Products page
- CRUD Users page
- Administrator / Dashboard Page

### Fixed
- Permission Roles Fix (Continuous Update)

